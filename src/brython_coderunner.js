function sanitizeHTML(text) {
    var element = document.createElement('div');
    element.innerText = text;
    return element.innerHTML;
}

function sanitizeHTML(text) {
    var element = document.createElement('div');
    element.innerText = text;
    return element.innerHTML;
}

function set_up(path, ) {


    function transform_codebox(path, options) {
        // Canvas
        $('.coding_box').each(function(i) {
            if (!$(this).hasClass("brython_coderunner_transformed")) {
                $(this).addClass("brython_coderunner_transformed")
                if (!$("body").hasClass("brocessing")) {
                    $("body").addClass("brocessing")
                }
                codingbox = $(this);
                if (codingbox.hasClass("buttons")) {
                    row = codingbox.find(".buttons_row")
                    row.append('<a class="btn run" role="button"><i class="fa fa-play"></i>Run</a>');
                    row.append('<a class="btn clear" role="button"><i class="fa fa-stop"></i>Stop\/Clear</a>');
                }

                // Console
                if (codingbox.hasClass("console")) {
                    row = codingbox.find(".console_row")
                    row.append('<div class="console-wrapper"></div>')
                    div = row.find(".console-wrapper")
                    div.append("<h3>Konsole</h3>")
                    div.append('<pre class="console"></pre>')
                }
                // Editor
                row = $(this).find(".editor_row")
                row.find(".editor").attr("id", "autoid_" + i);
                editor_id = row.find(".editor").attr("id");
                var editor = ace.edit(editor_id);
                editor.session.setValue($(codingbox.find(".code")).text().trim().replace(/(<([^>]+)>)/ig, ""));
                editor.setFontSize("18px");
                editor.session.setMode("ace/mode/python");
                editor.getSession().on('change', function() {
                    codingbox.find(".code_area").val(editor.getSession().getValue())
                    codingbox.find(".code").text(editor.getSession().getValue());
                });
                if (obj.hasOwnProperty("min_lines")) {
                    min_lines = parseInt(sanitizeHTML(obj["min_lines"]))
                } else {
                    min_lines = 10
                }
                if (obj.hasOwnProperty("max_lines")) {
                    max_lines = parseInt(sanitizeHTML(obj["max_lines"]))
                } else {
                    max_lines = 10
                }
                editor.setTheme("ace/theme/monokai");
                editor.setOptions({
                    minLines: min_lines,
                    maxLines: max_lines,
                    tabSize: 4,
                    useSoftTabs: true
                });
                if (codingbox.hasClass("editor")) {
                    row.show();
                }
            }
        });
    }

    function set_up_codingbox(codingbox, code) {
        codingbox.append('<div class="code_row"><textarea class="code" cols="40" style="display:none;"></textarea></div>\n' +
            '<div class="editor_row" style="display:none;"><div class="editor" cols="40" rows="10"></div></div>\n' +
            '<div class="buttons_row"></div>\n' +
            '<div class="canvas_row"><div class="spinner lds-ring"><div></div><div></div><div></div><div></div></div><div class="canvas" width="300px" height="400px" style="display:none;"></div>\n    </div>\n' +
            '<div class="console_row"></div>');
        $(this).hide();
        codingbox.parent().find(".code").text(code);
    }


    function set_up_code_field(code_field) {
        code_field.addClass("brython_coderunner_prepared");
        code_field.wrap("<div class='brython_codefield'></div>");
        parent_div = code_field.parent();
        lines = code_field.text().split('}')
        json_firstline = lines[0].trim() + "}";
        lines.splice(0, 1);
        code_string = lines.join('\n').replace(/.*/, "").substr(1);;
        code_field.hide()
        obj = JSON.parse(json_firstline);
        options = sanitizeHTML(code_field.attr('class')) + " " + sanitizeHTML(obj["options"]);
        codingbox = $('<div class="coding_box ' + options + '"></div>');
        $(parent_div).after(codingbox)
        set_up_codingbox(codingbox, code_string)
        transform_codebox(path)

    }

    function set_up_textarea_fields(parent_field) {
        parent_field.addClass("brython_coderunner_prepared");
        textarea = parent_field.find("textarea");
        json_text = textarea.clone().children().remove().end().text();
        obj = JSON.parse(json_text);
        code = textarea.val();
        options = sanitizeHTML(parent_field.attr('class')) + " " + sanitizeHTML(obj["options"]);
        codingbox = $('<div class="coding_box ' + options + '"></div>')
        parent_field.after(codingbox);
        set_up_codingbox(codingbox, code);
        transform_codebox(path);

    }


    $('code.language-brython_coderunner:not("brython_coderunner_prepared"), code.brython_coderunner:not("brython_coderunner_prepared")').each(function(i) {
        set_up_code_field($(this));
    });
    $('div.brython_textarea:not("brython_coderunner_prepared")').each(function(i) {
        set_up_textarea_fields($(this));
    });
}

function load_brython_code(path) {
    var s = document.createElement("script");
    s.type = "text/python";
    lastChar = path.substr(-1);
    if (lastChar != "/") {
        path = path + "/"
    }
    s.src = (path || "") + "lib/brython_coderunner.bry";
    // Use any selector
    $("head").append(s);
    console.info("start brython with path" + path + ", " + s.src);
    brython({
        pythonpath: [path + "lib"]
    });
}

function brython_coderunner(path, callback = false) {

    return new Promise(function(resolve, reject) {
        $.getMultiScripts = function(arr, path) {
            var _arr = $.map(arr, function(scr) {
                return $.getScript((path || "") + scr);
            });

            _arr.push($.Deferred(function(deferred) {
                $(deferred.resolve);
            }));

            return $.when.apply($, _arr);
        }

        var script_arr = [
            '/lib/brython/brython.min.js',
            "/lib/brython/brython_stdlib.js",
        ];

        $.getMultiScripts(script_arr, path).done(function() {
            $('head').append('<link rel="stylesheet" type="text/css" href="' + (path || "") + '/src/brython_coderunner.css' + '">');
            // console.info("@todo is p5 a functin?" + typeof p5);
            set_up(path)
            if (callback != false) {
                callback(add)
            }


            if ($("body").hasClass("brocessing")) {
                load_brython_code(path)
            }

            resolve("done");


        });


    });
}

// paths:  ["./jquery", "./ace", "./brython", "./brython_modules", "./p5"]

// Define a module "myModule" with two dependencies, jQuery and Lodash
if (typeof define !== "undefined") {
    define("brython_coderunner", function() {

        var brython_coderunner = {}

        brython_coderunner.version = 1

        return brython_coderunner

    });
}