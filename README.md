# Brython Coderunner

Brython Coderunner transforms your Markdown fields and textareas into a Brython IDE. You can execute code immediately and even use Processing (Brython+Processing = Brocessing).

## Usage

Currently there are the following ways to generate a Brython field:

### Code-Field

#### Prequesites

Brython-Coderunner is build on top of ace-Editor and p5 Library. So you need following imports:

```
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/ace-editor-builds@1.2.4/src-min-noconflict/ace.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/p5@1.3.0/lib/p5.min.js"></script>
    <script type="text/javascript" src="path-to-brython-coderunner/src/brython_coderunner.js"></script>
```

Code fields with the following classes are transformed in Brython-IDES:

```
<code class="language-brython_coderunner>
</code>


or simply

<code class="brython_coderunner">
</code>
```

Textareas can also be converted. To do this, the textareas must be enclosed by a div in the following form:

```
<div class="brython_textarea">
<textarea>
</textarea>
</div>
```
### Options

Options can be passed to the representation. The most elegant way to do this is to pass a JSON field right after the opening tag:
 
```
<code class="brython_coderunner">
{options="processing buttons editor"}
</code>
```

Possible options:

  * **editor**: Displays the editor.

  * **buttons** Displays buttons (Run, Clear).

  * **canvas** Displays a canvas where code can be rendered.

  * **brocessing** / **processing**: Loads the code as processing code.
  
  * **turtle** / Transforms import turtle in brython (loads libraries and turtle-canvas) (alpha)

#### Load Brython Coderunner

Coderunner can be loaded with following Code:

```
<!-- Brython / Processing -->
<script src="https://cdn.jsdelivr.net/npm/p5@1.3.0/lib/p5.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/brython_coderunner@1.2.3/src/brython_coderunner.js"></script>
<script>
    $(window).on('load', function() {
            brython_coderunner("/resources/brython_coderunner/").then(function() {
                console.info("then");
            }); // then
    }); // on
</script>
```
(Change version to actual version or use local path)

##### Moodle integration

You can use Brython Coderunner as moodle database activity or embed it with a moodle-text-block. See moodle directory.